﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace TimeTrees
{
    struct TimelineEvent
    {
        public DateTime theEvent;
        public string description;
    }

    struct Person
    {
        public int Id;

        public Person(int id, string name, DateTime birthDate, DateTime death)
        {
            Id = id;
            Name = name;
            BirthDate = birthDate;
            Death = death;
        }

        public string Name;

        public DateTime BirthDate;

        public DateTime Death;
    }

    class Program
    {
        const int PeopleIdIndex = 0;

        const int PeopleNameIndex = 1;

        const int BirthDayIndex = 2;

        const int DeathIndex = 3;

        const int theEventIndex = 0;

        const int descriptionEvent = 1;

        static void Main(string[] args)
        {
            string peopleJsonFile = "..\\..\\..\\..\\people.json";

            string timelineJsonFile = "..\\..\\..\\..\\timeline.json";

            (Person[] peopleJson, TimelineEvent[] timelineJson) = FromFileToArray(peopleJsonFile, timelineJsonFile);

            string timelineFile = "..\\..\\..\\..\\timeline.csv";

            string peopleFile = "..\\..\\..\\..\\people.csv";

            TimelineEvent[] timeline = ReadEventFile(timelineFile);

            Person[] people = ReadPeopleFile(peopleFile);

            (int year, int month, int day) = CountMinAndMaxData(timeline);

            Console.WriteLine($"Между событиями прошло: {year} лет, {month} месяцев, {day} дней");

            string man = CheckMan(people);

            Console.WriteLine($"Имена людей, которые родились в високосный год, и они моложе 20 лет: {man}");

            Console.ReadKey();
        }

        private static List<TimelineEvent> ReadEventJson(string file)
        {
            string[] text1 = File.ReadAllLines(file);

            for (int i = 2; i < text1.Length - 2; i += 4)
            {
                if (text1[i] == null) continue;
                string line = text1[i];
                string[] date = line.Split(":");
            }
            string text = String.Join(null, text1);
            return JsonConvert.DeserializeObject<List<TimelineEvent>>(text);
        }

        private static List<Person> ReadPeopleJson(string file)
        {
            string text = File.ReadAllText(file);
            string nullDate = "\"0001-01-01\" ";
            text = text.Replace("null", nullDate);
            return JsonConvert.DeserializeObject<List<Person>>(text);
        }

        static (Person[], TimelineEvent[]) FromFileToArray(string peopleFile, string timelineFile)
        {
            List<Person> textPeopleJson = ReadPeopleJson(peopleFile);
            Person[] people = new Person[textPeopleJson.Count];
            textPeopleJson.CopyTo(people);
            List<TimelineEvent> timelineEvent = ReadEventJson(timelineFile);
            TimelineEvent[] timeTrees = new TimelineEvent[timelineEvent.Count];
            timelineEvent.CopyTo(timeTrees);
            return (people, timeTrees);
        }

        static Person[] ReadPeopleFile(string text)
        {
            string[] lines = File.ReadAllLines(text);
            Person[] people = new Person[lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                string[] separation = line.Split(";");
                people[i].Id = Int32.Parse(separation[PeopleIdIndex]);
                people[i].Name = separation[PeopleNameIndex];
                people[i].BirthDate = CorrectDate(separation[BirthDayIndex]);
                if (separation.Length == 4)
                {
                    people[i].Death = DateTime.Parse(separation[DeathIndex]);
                }
            }
            return people;
        }

        static TimelineEvent[] ReadEventFile(string text)
        {
            string[] lines = File.ReadAllLines(text);
            TimelineEvent[] timelineEvent = new TimelineEvent[lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                string[] separation = line.Split(";");
                string date = separation[theEventIndex];
                timelineEvent[i].theEvent = CorrectDate(date);
                timelineEvent[i].description = separation[descriptionEvent];
            }
            return timelineEvent;
        }
        static DateTime CorrectDate(string someDate)
        {
            if (someDate.Length == 4)
                someDate += "-01";
            return DateTime.Parse(someDate);
        }
        static (int, int, int) CountMinAndMaxData(TimelineEvent[] Events)
        {
            DateTime minDate = DateTime.MaxValue;
            DateTime maxDate = DateTime.MinValue;
            for (int i = 0; i < Events.Length; i++)
            {
                if (Events[i].theEvent.CompareTo(maxDate) > 0)
                    maxDate = Events[i].theEvent;
                if (Events[i].theEvent.CompareTo(minDate) < 0)
                    minDate = Events[i].theEvent;
            }
            int CountDays = maxDate.Day - minDate.Day;
            int CountMonths = maxDate.Month - minDate.Month;
            int CountYears = maxDate.Year - minDate.Year;
            if (CountDays < 0)
            {
                CountDays += 31;
                CountMonths -= 1;
            }
            if (CountMonths < 0)
            {
                CountMonths += 12;
                CountYears -= 1;
            }
            return (CountYears, CountMonths, CountDays);
        }

        private static string CheckMan(Person[] people)
        {
            for (int i = 0; i < people.Length; i++)
            {
                DateTime birthDay = people[i].BirthDate;
                if (IsLeapYear(birthDay.Year)
                    && AgeUntilTwenty(birthDay))
                {
                    return (people[i].Name);
                }
            }
            return null;
        }

        static bool IsLeapYear(int year)
        {
            if ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static bool AgeUntilTwenty(DateTime birthDay)
        {
            DateTime today = DateTime.Today;
            if (today.Year - birthDay.Year == 20)
            {
                if ((today.Month < birthDay.Month)) return true;
                else
                {
                    return (today.Month == birthDay.Month) && (today.Day < birthDay.Day);
                }
            }
            return today.Year - birthDay.Year < 20;
        }
    }
}